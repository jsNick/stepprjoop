document.addEventListener('DOMContentLoaded', onReady);

let visit;
let found;

function onReady () {
    let modal = document.getElementById('modal');
    const topBtn = document.getElementById('topBtn');
    topBtn.addEventListener('click', function onTopBtnClick () {
        modal.classList.add('active');
    });
    const medicSelect = document.getElementById('medic-select');
    medicSelect.addEventListener('change', onMedicSelectChange);
    const createBtn = document.getElementById('create-btn');
    createBtn.addEventListener('click', onCreateBtnClick);
    const closeBtn = document.getElementById('closeBtn');
    closeBtn.addEventListener('click', function onCloseBtnClick () {
        modal.classList.remove('active');
    });
}

function onMedicSelectChange (e) {
    let target = e.target;
    let value = target.value;
    let form = document.getElementById('select-form');
    let input = document.querySelectorAll('input');
    if (value === 'no-doctor') {
        for (let inp of input) {
            form.removeChild(inp);
        }
    }
    found = DataWorker.doctorBlankData.find(elem => value === elem.id);
    for (let inp of input) {
        form.removeChild(inp);
    }
    found.fields.forEach(item => {
        let input = document.createElement('input');
        input.id = item.id;
        input.type = item.type;
        input.setAttribute('required', '');
        input.placeholder = item.title;
        form.append(input);
    });
}

function onCreateBtnClick () {
    let modal = document.getElementById('modal');
    let sentence = document.getElementById('sentence');
    modal.classList.remove('active');
    sentence.classList.add('optional');
    let d = new Date();
    visit = new Visit (Visit.getId(),found.title, d.toLocaleDateString(), document.getElementById('fullName').value);

    if (found.title === 'Кардиолог')  {
        let cardiolog = new Cardiologist('', '', document.getElementById('visitTarget').value,
            document.getElementById('ordinaryPressure').value, document.getElementById('weightIndex').value,
            document.getElementById('sufferedDiseases').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, cardiolog.Data);
    }

    if (found.title === 'Терапевт') {
        let therapist = new Therapist('', '', document.getElementById('visitTarget').value,
            document.getElementById('age').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, therapist.Data);
    }

    if (found.title === 'Стоматолог') {
        let dantist = new Dentist('', '', document.getElementById('visitTarget').value,
            document.getElementById('lastVisitDate').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, dantist.Data);
    }
}

